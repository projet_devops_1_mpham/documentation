# Bienvenue dans mon projet personnel !!! 

# Introduction

Bienvenue dans mon projet personnel DevOps. L'objectif de ce projet est de mettre en place une infrastructure réseau et une usine logicielle permettant de faire vivre une API durant sa phase de développement et de production. Le but de ce projet n'est donc pas de développer l'application en elle même, mais de créer les machines qui l'héberge et d'automatiser la création de l'infrastructure.


## Fiche d'identité du projet

Intitulé du projet : Projet_DevOps_1

Contexte :\
    Objectifs principaux : 

        - Apprendre et développer des compétences sur des outils DevOps.
        - Mettre en place une infrastructure réseau et une usine logicielle qui permet d'intégrer et de déployer une application de bout en bout.
        - Capitaliser sur des méthodes de travail et des outils DevOps.
        - Former un dépôt Git constitué d'outils, de services et d'informations réutilisables.

Périmètre fonctionnel : 

    - Ce projet n'a pas pour objectif d'être utilisé fonctionnellement. Une fois terminé il pourra servir de boite à outil.

Utilisateurs finaux :

    - En temps que boite à outil, ce projet public pourra servir tous les utilisateurs qui en ont besoin.

Opportunités :

    - Apprendre de nouveaux concepts DevOps et approfondir des compétences existantes.

Contraintes :
    Matérielles :

        - L'infrastructure développée sera hébergée sur un unique ordinateur. Tout se fera donc en local avec des limitations en RAM et CPU.
    Temporelles :

        - Ce projet se développe sur mon temps libre, plusieurs dizaines d'heures seront nécéssaires pour le développer, c'est pour cela qu'il s'étend sur plusieurs mois.

Risques :

    - Le développement en local sur une unique machine, n'est pas représentatif du monde de l'entreprise.

# Présentation du matériel

Le PC qui sera en charge d'héberger l'application est doté d'un OS Windows 11 avec 16 Go de RAM et 8 cpu.


# Définition de l'infrastructure réseau


L'infrastructure des machines virtuelles seront donc supportées par une unique machine hôte Windows 10.
Un des premiers objectif de ce projet est de mettre en place une infrastructure réseau qui soit maintenable et scalable.
Une bonne pratique pour cela est de mettre en place une "infrastructure as code".
Dans mon environnement j'ai donc fait le choix d'utiliser vagrant, shell et ansible pour créer et provisionner les machines qui seront créés. 
Dans le cadre du projet nous utiliserons une infrastructure complète basé sur du Linux, ma machine Windows seront donc seulement nécessaire pour créer une première machine virtuelle (VM) linux qui servira à provisionner toutes les autres VM du projet.

## Présentation de l'applicatif


# Création de la machine qui provisionne l'infrastructure.

La machine qui provisionne l'infrastructure permettra de créer à l'aide de Vagrant et d'Ansible une série de machines virtuelles qui hébergeront l'application mais également les outils nécessaires au fonctionnement de l'usine logicielle. La machine qui provisione l'infrastructure sera nommée "vm-provisioner" et sera créee à l'aide de l'outil VirtualBox. Ce sera une machine Linux avec un OS Ubuntu.

Les images ubuntu peuvent être récuprérés sur le lien suivant : https://ubuntu.com/download/server

Nous utiliserons l'image : Ubuntu Server 22.04.3 LTS

## Paramètres VirtualBox :

Les paramètres suivant doivent être sélectionnés, le reste peut rester par défaut.

### Général

Nom : vm-provisioner
Type : Linux
Version : Ubuntu (64-bit)

Presse-papier partagé : bidirectionnel
Glisser-Déposer : bidirectionnel

### Système

Mémoire vive : 2048 Mb
Processeurs : 2 cpu
Stockage : 32Go

### Affichage

Mémoire Vidéo : 128 Mb

### Stockage

### Son

Désactivé

### Réseau

NAT
 
## Activer la virtualisation imbriquée 

Dans l'onglet system -> processeur -> cocher la case "Activer VT-x/AMD-V" imbriqué

## Copie du dossier partagé dans les VM

- cd
- cd ..
- cp -r /media/sf_Projet_perso_DevOps /home/matthieu/Documents

## Afficher les ip dispo en ssh

- grep -ri ip:

## Connection à la base de donnée posgres sql

- psql -h [IP] -U [user] [bdd_name]







